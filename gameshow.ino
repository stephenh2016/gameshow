
int pinA = 2;
int pinB = 3;
int pinC = 4;
int pinD = 5;
int pinE = 6;
int pinF = 7;
int pinG = 8;
int D1 = 9;
int D2 = 10;
int D3 = 11;
int D4 = 12;
int a=-1;
int b=-1;
int c=-1;

void setup() {
  // put your setup code here, to run once:
  pinMode(pinA, OUTPUT);     
  pinMode(pinB, OUTPUT);     
  pinMode(pinC, OUTPUT);     
  pinMode(pinD, OUTPUT);     
  pinMode(pinE, OUTPUT);     
  pinMode(pinF, OUTPUT);     
  pinMode(pinG, OUTPUT);   
  pinMode(D1, OUTPUT);  
  pinMode(D2, OUTPUT);  
  pinMode(D3, OUTPUT);  
  pinMode(D4, OUTPUT);  

  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);

  digitalWrite(D4, LOW);
  digitalWrite(D3, LOW);
  digitalWrite(D2, LOW);
  digitalWrite(D1, LOW);
}

void displayReset(){
    digitalWrite(D1, HIGH);
    digitalWrite(pinA, HIGH);   
    digitalWrite(pinB, HIGH);   
    digitalWrite(pinC, HIGH);   
    digitalWrite(pinD, HIGH);   
    digitalWrite(pinE, HIGH);   
    digitalWrite(pinF, HIGH);   
    digitalWrite(pinG, HIGH);
    digitalWrite(D1, LOW);  
    digitalWrite(D2, HIGH);
    digitalWrite(pinA, HIGH);   
    digitalWrite(pinB, HIGH);   
    digitalWrite(pinC, HIGH);   
    digitalWrite(pinD, HIGH);   
    digitalWrite(pinE, HIGH);   
    digitalWrite(pinF, HIGH);   
    digitalWrite(pinG, HIGH);
    digitalWrite(D2, LOW);  
    digitalWrite(D3, HIGH);
    digitalWrite(pinA, HIGH);   
    digitalWrite(pinB, HIGH);   
    digitalWrite(pinC, HIGH);   
    digitalWrite(pinD, HIGH);   
    digitalWrite(pinE, HIGH);   
    digitalWrite(pinF, HIGH);   
    digitalWrite(pinG, HIGH);
    digitalWrite(D3, LOW);  
    digitalWrite(D4, HIGH);
    digitalWrite(pinA, LOW);   
    digitalWrite(pinB, LOW);   
    digitalWrite(pinC, LOW);   
    digitalWrite(pinD, LOW);   
    digitalWrite(pinE, LOW);   
    digitalWrite(pinF, LOW);   
    digitalWrite(pinG, LOW);
    delay(1000);
    digitalWrite(D4, LOW);
}
void disp(int val,int pos,int del){
  if(pos==0){
    digitalWrite(D1,HIGH);
  }
  else if(pos==1){
    digitalWrite(D2,HIGH);
  }
  else if(pos==2){
    digitalWrite(D3,HIGH);
  }
  if(val==0){
    digitalWrite(pinA, LOW);   
    digitalWrite(pinB, LOW);   
    digitalWrite(pinC, LOW);   
    digitalWrite(pinD, LOW);   
    digitalWrite(pinE, LOW);   
    digitalWrite(pinF, LOW);   
    digitalWrite(pinG, HIGH); 
  }
  else if(val==1){
    digitalWrite(pinA, HIGH);   
    digitalWrite(pinB, LOW);   
    digitalWrite(pinC, LOW);   
    digitalWrite(pinD, HIGH);   
    digitalWrite(pinE, HIGH);   
    digitalWrite(pinF, HIGH);   
    digitalWrite(pinG, HIGH);     
  }
  else if(val==2){
    digitalWrite(pinA, LOW);   
    digitalWrite(pinB, LOW);   
    digitalWrite(pinC, HIGH);   
    digitalWrite(pinD, LOW);   
    digitalWrite(pinE, LOW);   
    digitalWrite(pinF, HIGH);   
    digitalWrite(pinG, LOW);     
  }
  delay(del);
  if(pos==0){
    digitalWrite(D1,LOW);
  }
  else if(pos==1){
    digitalWrite(D2,LOW);
  }
  else if(pos==2){
    digitalWrite(D3,LOW);
  }  
}


void loop() {
  // put your main code here, to run repeatedly:
  
  if(digitalRead(A0) == LOW){
    if(a==-1){
      a=0;
    }
    else if(a!=0 && b==-1){
      b=0;
    }
    else if(a!=0 && b!=0 && c==-1){
      c=0; 
    }

  }
  else if(digitalRead(A1) == LOW){
    if(a==-1){
      a=1;
    }
    else if(a!=1 && b==-1){
      b=1;
    }
    else if(a!=1 && b!=1 && c==-1){
      c=1; 
    }
  }
  else if(digitalRead(A2) == LOW){
    if(a==-1){
      a=2;
    }
    else if(a!=2 && b==-1){
      b=2;
    }
    else if(a!=2 && b!=2 && c==-1){
      c=2; 
    }
  } 
  
  else if(digitalRead(A3) == LOW){
    a = -1;
    b = -1;
    c = -1;
    displayReset();
  }
  if(a!=-1){
    disp(a,0,1);
  }
  if(b!=-1){
    disp(b,1,1);
  }
  if(c!=-1){
    disp(c,2,1);
  }
}
