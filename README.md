# Game Show controller

## Aim:

Create a controller to simulate game show buzzers.
I don't like buzzers so am using a 7 segment display to see who buzzes
in 1,2 and 3rd.

## schematic in kicad
These are the .pro and .sch files

## Arduino uno code
this is in the .ino file

## Stand and pipe caps
these are the free cad files .fcstd

----

see [wiki](https://bitbucket.org/stephenh2016/gameshow/wiki/Home) for more details
